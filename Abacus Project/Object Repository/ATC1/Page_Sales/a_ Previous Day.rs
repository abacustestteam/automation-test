<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_ Previous Day</name>
   <tag></tag>
   <elementGuidId>13090fd0-b88d-4dc6-a525-dcde25ef5d68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@onclick = 'addDay(1, false)' and (text() = '&lt; Previous Day' or . = '&lt; Previous Day')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;day_prevDay&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>addDay(1, false)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>&lt; Previous Day</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;day&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8 text-right&quot;]/div[@class=&quot;date-nav&quot;]/div[@class=&quot;date-nav-top&quot;]/a[2]</value>
   </webElementProperties>
</WebElementEntity>
