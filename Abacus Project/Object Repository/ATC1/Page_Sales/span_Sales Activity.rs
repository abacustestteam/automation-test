<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Sales Activity</name>
   <tag></tag>
   <elementGuidId>47eed35f-c494-432a-a68e-5d8ad8e92d07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;M150S0&quot;)/li[2]/a[1]/span[1][count(. | //span[(text() = 'Sales Activity' or . = 'Sales Activity')]) = count(//span[(text() = 'Sales Activity' or . = 'Sales Activity')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sales Activity</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;M150S0&quot;)/li[2]/a[1]/span[1]</value>
   </webElementProperties>
</WebElementEntity>
