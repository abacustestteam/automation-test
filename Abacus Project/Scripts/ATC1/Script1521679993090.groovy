import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.callTestCase(findTestCase('TC1_VerifySuccessfulLogin'), [:], FailureHandling.STOP_ON_FAILURE)

for (def index : (0..6)) {
    WebUI.waitForElementVisible(findTestObject('Page_Sales/a_ Previous Day'), 5)

    WebUI.click(findTestObject('ATC1/Page_Sales/a_ Previous Day'))

    WebUI.waitForElementVisible(findTestObject('ATC1/Page_Sales/div_664.15'), 5)

    DashboardValue = WebUI.getText(findTestObject('ATC1/Page_Sales/div_664.15'))

    WebUI.click(findTestObject('ATC1/Page_Sales/a_Reports'))

    WebUI.waitForElementVisible(findTestObject('Page_Sales/span_Sales Activity'), 5)

    WebUI.click(findTestObject('ATC1/Page_Sales/span_Sales Activity'))

    WebUI.waitForElementVisible(findTestObject('Page_Sales/a_ Previous Day'), 5)

    WebUI.click(findTestObject('ATC1/Page_Sales Activity/a_ Previous Day'))

    WebUI.waitForElementVisible(findTestObject('ATC1/Page_Sales Activity/th_664.15'), 3)

    SalesActivityValue = WebUI.getText(findTestObject('ATC1/Page_Sales Activity/th_664.15'))

    WebUI.click(findTestObject('ATC1/Page_Sales Activity/span_Sales Summary'))

    WebUI.waitForElementVisible(findTestObject('Page_Sales Summary By Store/button_ Previous Day'), 5)

    WebUI.click(findTestObject('ATC1/Page_Sales Summary By Store/button_ Previous Day'))

    WebUI.waitForElementVisible(findTestObject('ATC1/Page_Sales Summary By Store/td_664.15'), 10)

    SalesSummaryValue = WebUI.getText(findTestObject('ATC1/Page_Sales Summary By Store/td_664.15'))

    'Cross Check the Dashboard Sales Value & Sales Activity Value( Incl. GST)'
    WebUI.verifyEqual(DashboardValue, SalesActivityValue)

    'If the values match --> Perform the below operations'
    if (true) {
        'Cross Check the Dashboard Sales Value & Sales Summary Value i.e Gross Value'
        WebUI.verifyEqual(DashboardValue, SalesSummaryValue)

        if (false) {
            throw new com.kms.katalon.core.exception.StepFailedException()
        }
    } else {
        'Error Message to denote that the values of Dasboard Value and Sales Activity dont match'
        throw new com.kms.katalon.core.exception.StepFailedException(' Values dont match !')
    }
}

