import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Open the Chrome Browser'
WebUI.openBrowser('')

WebUI.callTestCase(findTestCase('TC1_VerifySuccessfulLogin'), [:], FailureHandling.STOP_ON_FAILURE)

'Click on Previous Day'
WebUI.click(findTestObject('Page_Sales/a_ Previous Day'))

'Provided Timeout to capture the Dashboard Sales Value'
WebUI.waitForElementVisible(findTestObject('Page_Sales/div_110.05'), 5)

'Capture the Dashboard Sales Value'
Dashboard_Value = WebUI.getText(findTestObject('Page_Sales/div_110.05'))

'Click on Reports Tab'
WebUI.click(findTestObject('Page_Sales/a_Reports'))

'Under Reports Tab --> Click on Sales Activity'
WebUI.click(findTestObject('Page_Sales/span_Sales Activity'))

'Click on Previous Day'
WebUI.click(findTestObject('Page_Sales Activity/a_ Previous Day'))

'Capture the Sales Activity Value'
Sales_Activity_Value = WebUI.getText(findTestObject('Page_Sales Activity/th_110.05'))

'Under Reports Tab --> Click on Sales Summary'
WebUI.click(findTestObject('Page_Sales Activity/span_Sales Summary'))

'Click on Previous Day'
WebUI.click(findTestObject('Page_Sales Summary By Store/button_ Previous Day'))

WebUI.waitForElementVisible(findTestObject('Page_Sales Summary By Store/td_110.05'), 5)

'Capture the Sales Summary Value'
Sales_Summary_Value = WebUI.getText(findTestObject('Page_Sales Summary By Store/td_110.05'))

try {
    'Cross Check the Dashboard Sales Value & Sales Activity Value( Incl. GST)'
    WebUI.verifyEqual(Dashboard_Value, Sales_Activity_Value)

    'If the values match --> Perform the below operations'
    if (true) {
        'Cross Check the Dashboard Sales Value & Sales Summary Value i.e Gross Value'
        WebUI.verifyEqual(Dashboard_Value, Sales_Summary_Value)

        if (false) {
            throw new com.kms.katalon.core.exception.StepFailedException()
        }
    } else {
        'Error Message to denote that the values of Dasboard Value and Sales Activity dont match'
        throw new com.kms.katalon.core.exception.StepFailedException(' Values dont match !')
    }
}
catch (Exception e) {
    println(e.printStackTrace())
} 
finally { 
    'Close the Chrome Browser'
    WebUI.closeBrowser()
}

